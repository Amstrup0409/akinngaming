<!DOCTYPE html>
<html lang="da-dk">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>AkinnGaming - For a better gaming experience</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/agency.css" rel="stylesheet">

  </head>

  <body id="page-top">

    <!-- Navigation -->
    <?php include 'nav-bar.php' ?>

    <!-- Header -->
    <header class="masthead">
      <div class="container">
        <div class="intro-text">
          <div class="intro-heading text-uppercase">Om os</div>
          <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="#services">Læs mere</a>
        </div>
      </div>
    </header>

    <!-- About -->
    <section id="about">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">Hvem vi er</h2>
            <h3 class="section-subheading text-muted">Fra start til endnu en start...</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <ul class="timeline">
              <li>
                <div class="timeline-image">
                  <img class="rounded-circle img-fluid" src="img/about/idea.jpg" alt="">
                </div>
                <div class="timeline-panel">
                  <div class="timeline-heading">
                    <h4>Marts 2017</h4>
                    <h4 class="subheading">Fra idé til handling</h4>
                  </div>
                  <div class="timeline-body">
                    <p class="text-muted">Patrick fik en idé - en god idé. Han ville lave et online gaming community for LoL spillere. Martin blev spurgt om han ville være med. Begge kunne se et hul i markedet og et kæmpe potentiale for at være dem, der skulle dække det.</p>
                  </div>
                </div>
              </li>
              <li class="timeline-inverted">
                <div class="timeline-image">
                  <img class="rounded-circle img-fluid" src="img/about/action.jpg" alt="">
                </div>
                <div class="timeline-panel">
                  <div class="timeline-heading">
                    <h4>Februar 2018</h4>
                    <h4 class="subheading">Fra handling til endnu mere handling</h4>
                  </div>
                  <div class="timeline-body">
                    <p class="text-muted">Kursen var sat. Nu var det bare at komme igang. Der blev næsten arbejdet uafbrudt mandag til fredag fra 10.00 - 18.00 i juni og juli måned for at komme op med et eksistensgrundlag, et navn, en strategi og fremtidig plan.</p>
                  </div>
                </div>
              </li>
              <li>
                <div class="timeline-image">
                  <img class="rounded-circle img-fluid" src="img/about/hire.jpg" alt="">
                </div>
                <div class="timeline-panel">
                  <div class="timeline-heading">
                    <h4>Juli 2018</h4>
                    <h4 class="subheading">Første ansættelse... stadig uden løn</h4>
                  </div>
                  <div class="timeline-body">
                    <p class="text-muted">Christian Amstrup - en guildie fra World of Warcraft, som Martin havde spillet sammen med i flere år, blev headhuntet til projektet. Nu var de sidste brikker i puslespillet lagt, og fokus blev nu at føre det ud i livet.</p>
                  </div>
                </div>
              </li>
              <li class="timeline-inverted">
                <div class="timeline-image">
                  <img class="rounded-circle img-fluid" src="img/about/forward.jpg" alt="">
                </div>
                <div class="timeline-panel">
                  <div class="timeline-heading">
                    <h4>September 2018</h4>
                    <h4 class="subheading">Hvor vi er nu...</h4>
                  </div>
                  <div class="timeline-body">
                    <p class="text-muted">En komplet og fuldt funktionel virksomhed med en drivkraft som ingen andre på markedet. Video, podcasts, view parties og et community, som ingen andre. Et team med utrolige kompetencer og forudsætninger for at gøre AkinnGaming endnu større!</p>
                  </div>
                </div>
              </li>
              <li class="timeline-inverted">
                <a href="upload.php">
                <div class="timeline-image">
                  <h4>BLIV EN
                    <br>DEL AF VORES
                    <br>HISTORIE!</h4>
                </div>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </section>

    <hr>
      
    <!-- Team -->
    <section class="" id="team">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">Vores ambitiøse team</h2>
            <h3 class="section-subheading text-muted">Skøre typer og kreative hjerner. Tilsammen er vi AkinnGaming.</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-4">
            <div class="team-member">
              <img class="mx-auto rounded-circle" src="img/team/patrick_u.jpg" alt="patrick udengaard">
              <h4>Patrick "Pallepaude" Udengaard</h4>
              <p class="text-muted">Partner & Content Resource Manager</p>
              <ul class="list-inline social-buttons">
                <li class="list-inline-item">
                  <a href="mailto:patrick.udengaard@akinngaming.com">
                    <i class="fas fa-envelope"></i>
                  </a>
                </li>
                <li class="list-inline-item">
                  <a href="https://www.linkedin.com/in/patrickudengaard/" target="_blank">
                    <i class="fab fa-linkedin"></i>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="team-member">
              <img class="mx-auto rounded-circle" src="img/team/martin_t.jpg" alt="martin tuxen qvistgaard">
              <h4>Martin "Mah_dudu" Tuxen Qvistgaard</h4>
              <p class="text-muted">Partner & Digital Marketing Strategist</p>
              <ul class="list-inline social-buttons">
                <li class="list-inline-item">
                  <a href="mailto:martin.tuxen@akinngaming.com">
                    <i class="fas fa-envelope"></i>
                  </a>
                </li>
                <li class="list-inline-item">
                  <a href="https://www.linkedin.com/in/martintuxen/" target="_blank">
                    <i class="fab fa-linkedin"></i>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="team-member">
              <img class="mx-auto rounded-circle" src="img/team/christian_a.jpg" alt="christian amstrup pedersen">
              <h4>Christian "Kætterfletter" Amstrup Petersen</h4>
              <p class="text-muted">Lead Back-end Developer</p>
              <ul class="list-inline social-buttons">
                <li class="list-inline-item">
                  <a href="mailto:christian.amstrup@akinngaming.com">
                    <i class="fas fa-envelope"></i>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="team-member">
              <img class="mx-auto rounded-circle" src="img/team/frederik_w.jpeg" alt="frederik wandahl warming">
              <h4>Frederik "AtomicWarming" Wandahl Warming</h4>
              <p class="text-muted">SoMe & SEO Specialist</p>
              <ul class="list-inline social-buttons">
                <li class="list-inline-item">
                  <a href="mailto:frederik.warming@akinngaming.com">
                    <i class="fas fa-envelope"></i>
                  </a>
                </li>
                <li class="list-inline-item">
                  <a href="https://www.linkedin.com/in/frederik-wandahl-warming-2979793a/" target="_blank">
                    <i class="fab fa-linkedin"></i>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="team-member">
              <img class="mx-auto rounded-circle" src="img/team/oliver_p.jpeg" alt="oliver panthera">
              <h4>Oliver "OllieG_93" Panthera</h4>
              <p class="text-muted">Content Creator</p>
              <ul class="list-inline social-buttons">
                <li class="list-inline-item">
                  <a href="mailto:oliver.panthera@akinngaming.com">
                    <i class="fas fa-envelope"></i>
                  </a>
                </li>
                <li class="list-inline-item">
                  <a href="https://www.linkedin.com/in/oliver-panthera/" target="_blank">
                    <i class="fab fa-linkedin"></i>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
    

    <!-- Footer -->
    <?php include 'footer.php' ?>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Contact form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/agency.min.js"></script>
    
    <!-- Carousel scripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>

  </body>

</html>
