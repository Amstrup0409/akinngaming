<!DOCTYPE html>
<html lang="da-dk">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>AkinnGaming - For a better gaming experience</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/agency.css" rel="stylesheet">

  </head>

  <body id="page-top">

    <!-- Navigation -->
    <?php include 'nav-bar.php' ?>

    <!-- Header -->
    <header class="masthead">
      <div class="container">
        <div class="intro-text">
          <h1 class="intro-heading text-uppercase">Privacy Policy</h1>
          <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="#services">Læs mere</a>
        </div>
      </div>
    </header>

    <!-- Privacy Policy -->
    <section class="bg-light" id="privacy-policy">
        <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="section-heading text-center">Privacy Policy</h2>
                <h3 class="section-subheading col-lg-12 text-muted text-center">Nedenfor kan du læse nærmere om, hvad dine rettigheder er ift. opbevaring og anvendelse af dine data, når du uploader til og anvender hjemmesiden hos AkinnGaming.</h3>
                <h4 class="section-subheading text-center mb-3">Beskyttelse af og fortrolighed med dine data</h4>
                <p>Når du registrerer oplysninger hos os, er vi ene ansvarlige for at beskytte og fortroligholde dine data i henhold til <a href="https://gdpr.dk/" target="_blank">GDPR lovgivningen</a>.</p>
                <ul>
                  <li>
                    <p>Du har som bruger af hjemmesiden ret til at tilgå, tilbagetrække og indhente information om de data, vi har om dig, og hvad formålet med indsamlingen af data er. Når du uploader videomateriale eller videregiver personlige data, kan du efterfølgende få tilsendt alle data, vi har på dig, ved at kontakte os <a href="mailto:data@akinngaming.com?Subject=DATA:%20Vedr.%20dataindsamling%20på%20Jeres%20hjemmeside">her</a>.</p>
                  </li>
                  <li>
                    <p>Når du giver samtykke til, at vi må opbevare dine data, giver du også samtykke til at vi må anvende eventuelt uploaded videomateriale til kommercielt brug. De personlige data, du giver os i udtrykt fortrolighed, videregives ikke - og vil aldrig blive det.</p>
                  </li>
                  <li>
                    <p>Dine data slettes <i>ikke</i> umiddelbart efter anvendelsen af dit uploadede materiale. Vi forbeholder os retten til at opbevare dine oplysninger og dit videomateriale i 6-12 måneder i henhold til eventuel udarbejdelse af analyser, statistikker og indhold til hjemmesiden på et senere tidspunkt. Ønsker du dine data fjernet før tid, kan du kontakte os <a href="mailto:data@akinngaming.com?Subject=DATA:%20Vedr.%20fjernelse%20af%20data">her</a>, hvorefter vi fjerner dine data og underretter dig hurtigst muligt.</p>
                  </li>
                  <li>
                    <p>Vi anvender cookies på vores hjemmeside og registrerer demografiske og geografiske oplysninger samt brugernes adfærd på hjemmesiden via Google Analytics. Vi henviser til <a href="https://support.google.com/analytics/answer/7105316?hl=da" target="_blank">Googles Privacy Policy</a>.</p>
                  </li>
                  <li>
                    <p>Vi er forpligtet og motiveret til at sikre dine personlige data, men kan ikke garantere sikkerheden for alle data, der registreres (ligesom på alle andre hjemmesider). Ved at dele personlige data med os, anerkender du denne risiko.</p>
                  </li>
                  <li>
                    <p>Hvis du mener, at vores behandling af dine oplysninger ikke lever op til lovgivningen, har du mulighed for at klage til Datatilsynet <a href="https://www.datatilsynet.dk/generelt-om-databeskyttelse/klage-til-datatilsynet/" target="_blank">her</a>.</p>
                  </li>
                </ul>
                <p class="mt-5">Har du spørgsmål, ris, ros eller kommentarer til ovenstående, kan du kontakte de dataansvarlige - Patrick Udengaard og Martin Tuxen - på <a href="mailto:data@akinngaming.com">data@akinngaming.com</a>.</p>
            </div>
        </div>
        </div>
    </section>

    <!-- Footer -->
    <?php include 'footer.php' ?>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Contact form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/agency.min.js"></script>
    
    <!-- Carousel scripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>

  </body>

</html>
