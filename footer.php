<!-- Footer -->
<footer class="page-footer font-small indigo bg-light border-top">
    
    <!-- Footer Links -->
    <div class="container">

      <!-- Grid row-->
      <div class="row text-center d-flex justify-content-center pt-4 mb-4">

        <!-- Grid column -->
        <div class="col-md-2 mb-3">
          <h6 class="text-uppercase font-weight-bold">
            <a href="om-os.php">Om os</a>
          </h6>
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-2 mb-3">
          <h6 class="text-uppercase font-weight-bold">
            <a href="company-news.php">Company news</a>
          </h6>
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <!--
        <div class="col-md-2 mb-3">
          <h6 class="text-uppercase font-weight-bold">
            <a href="faq.php">FAQ</a>
          </h6>
        </div>
        -->

        <!-- Grid column -->
        <div class="col-md-2 mb-3">
          <h6 class="text-uppercase font-weight-bold">
            <a href="privacy-policy.php">Privacy Policy</a>
          </h6>
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-2 mb-3">
          <h6 class="text-uppercase font-weight-bold">
            <a href="kontakt.php">Kontakt</a>
          </h6>
        </div>
        <!-- Grid column -->

      </div>
      <!-- Grid row-->
      <hr class="rgba-white-light" style="margin: 0 15%;">

      <!-- Grid row-->
      <div class="row d-flex text-center justify-content-center mb-md-0 mb-4">

        <!-- Grid column -->
        <div class="col-md-8 col-12 mt-5">
          <p style="line-height: 1.7rem">Skabt af et passioneret team, primært bestående af gaming entusiaster, er AkinnGaming en online platform og community, der skaber udbytterige services og værktøjer til gamere.</p>
        </div>
        <!-- Grid column -->

      </div>
      <!-- Grid row-->
      <hr class="clearfix d-md-none rgba-white-light" style="margin: 10% 15% 5%;">

      <!-- Grid row-->
      <div class="row pb-3">

        <!-- Grid column -->
        <div class="col-md-12">

          <div class="mb-2 mt-4 flex-center">

            <!-- YouTube -->
            <a class="" href="https://www.youtube.com/channel/UCTYdEYEJlKnYg5UczJ1nMOA?" target="_blank">
              <i class="fab fa-youtube fa-lg white-text mr-4"> </i>
            </a>
            <!-- Facebook -->
            <a class="" href="https://www.facebook.com/akinngaming/" target="_blank">
              <i class="fab fa-facebook fa-lg white-text mr-4"> </i>
            </a>
            <!-- Instagram -->
            <a class="" href="https://www.instagram.com/akinngaming/" target="_blank">
              <i class="fab fa-instagram fa-lg white-text mr-4"> </i>
            </a>
            <!-- SoundCloud -->
            <a class="" href="https://www.soundcloud.com/akinngaming/" target="_blank">
              <i class="fab fa-soundcloud fa-lg white-text mr-4"> </i>
            </a>
            <!-- Discord -->
            <a class="" href="https://discordapp.com/invite/Qe8XgGA" target="_blank">
              <i class="fab fa-discord fa-lg white-text mr-4"> </i>
            </a>
            <!-- Twitch -->
            <a class="" href="https://www.twitch.tv/akinngaming" target="_blank">
              <i class="fab fa-twitch fa-lg white-text"> </i>
            </a>

          </div>

        </div>
        <!-- Grid column -->

      </div>
      <!-- Grid row-->

    </div>
    <!-- Footer Links -->

    <!-- Copyright -->
    <hr>
    <div class="footer-copyright text-center py-3">2018 © AkinnGaming 2018. All rights reserved.</a>
    </div>
    <!-- Copyright -->

  </footer>
  <!-- Footer -->