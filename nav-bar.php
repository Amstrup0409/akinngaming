<nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="index.php">AkinnGaming</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav text-uppercase ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="fortnite.php">Fortnite</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="league-of-legends.php">League of Legends</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="podcasts.php">Podcasts</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="upload.php">Del dine plays!</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>