<!DOCTYPE html>
<html lang="da-dk">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>AkinnGaming - For a better gaming experience</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/agency.css" rel="stylesheet">

  </head>

  <body id="page-top">

    <!-- Navigation -->
    <?php include 'nav-bar.php' ?>

    <!-- Header -->
    <header class="masthead">
      <div class="container">
        <div class="intro-text">
          <div class="intro-heading text-uppercase">AkinnGaming</div>
          <div class="intro-lead-in">Keep on gaming!</div>
          <a class="btn btn-primary btn-xl text-uppercase" href="om-os.php">Hvem vi er</a>
        </div>
      </div>
    </header>

    <!-- Clients -->
    <?php include 'partners.php' ?>
      
    <!-- Flavour of the Month -->
    <section class="fotm">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">Flavour of the Month</h2>
            <h3 class="section-subheading text-muted">Månedens udvalgte højdepunkt</h3>
            <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="league-of-legends.php">Se video</a>
          </div>
        </div>
      </div>
    </section>
      
    <!-- Algoritme -->
    <section class="algorythm">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">Algorythm</h2>
            <h3 class="section-subheading text-muted">Personligt udvalgt til dig!</h3>
            <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="fortnite.php">Se video</a>
          </div>
        </div>
      </div>
    </section>

    <!-- Footer -->
    <?php include 'footer.php' ?>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Contact form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/agency.min.js"></script>
    
    <!-- Carousel scripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>

  </body>

</html>
