<!DOCTYPE html>
<html lang="da-dk">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>AkinnGaming - For a better gaming experience</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/agency.css" rel="stylesheet">

  </head>

  <body id="page-top">

    <!-- Navigation -->
    <?php include 'nav-bar.php' ?>

    <!-- Header -->
    <header class="masthead upload">
      <div class="container">
        <div class="intro-text">
          <div class="intro-heading text-uppercase"><img class="img-fluid" src="img/del-dine-plays-bg.png"></div>
          <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="#upload">Upload dine plays</a>
        </div>
      </div>
    </header>
    
    <?php include 'partners.php' ?>
    
    <hr>
    
    <!-- Upload form -->
    <div class="container pt-5 mb-5" id="upload">
        <h2 class="mb-5">Upload dine plays!</h2>
        <form action="upload_File.php" method="post" enctype="multipart/form-data"> 
            <div class="form-group">
                <input class="form-control" type="hidden" name="MAX_FILE_SIZE" value="512000" required />
            </div>
            <div class="form-group">
                <input name="userFile" type="file" text="Vælg fil" />
            </div>
            <div class="form-group">
                <input class="form-control" name="firstname" type="text" placeholder="Fornavn *" required />
            </div>
            <div class="form-group">
                <input class="form-control" name="lastname" type="text" placeholder="Efternavn *" required />
            </div>
            <div class="form-group">
                <input class="form-control" name="gamertag" type="text" placeholder="Gamertag *" required />
            </div>
            <div class="form-group">
                <input class="form-control" name="email" type="text" placeholder="Email *" required />
            </div>
            <h5 class="mt-5 mb-3">Hvilket spil omhandler dit klip?</h5>
            <div class="form-group">
                <input type="radio" name="game" type="text" required > Fortnite
            </div>
            <div class="form-group">
                <input type="radio" name="game" type="text" > League of Legends
            </div>
            <h5 class="mt-5 mb-3">Hvor foretrækker du at få dine gaming nyheder?</h5>
            <div class="form-check">
                <label class="form-check-label">
                    <div class="form-group">
                        <input class="form-check-input" type="checkbox" value=""> Websites
                    </div>
                    <div class="form-group">
                        <input class="form-check-input" type="checkbox" value=""> YouTube
                    </div>
                    <div class="form-group">
                        <input class="form-check-input" type="checkbox" value=""> Facebook
                    </div>
                    <div class="form-group">
                        <input class="form-check-input" type="checkbox" value=""> Instagram
                    </div>
                    <div class="form-group">
                        <input class="form-check-input" type="checkbox" value=""> Podcasts
                    </div>
                    <div class="form-group">
                        <input class="form-check-input" type="checkbox" value=""> Discord
                    </div>
                    <div class="form-group">
                        <input class="form-check-input" type="checkbox" value=""> Andre steder - hvor?
                        <input class="form-check-input" type="text" >
                    </div>
                </label>
            </div>
            <hr class="mt-5">
            <div class="form-group mt-">
                <input type="checkbox" name="accept" type="text" required > <i>Jeg har læst, forstået og accepterer AkinnGamings <a href="privacy-policy.php">privacy policy</a>.</i>
            </div>
            <div class="form-group mt-4">
                <input type="submit" value="Upload Video">
            </div>
        </form>
    </div>

    <!-- Footer -->
    <?php include 'footer.php' ?>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Contact form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/agency.min.js"></script>
    
    <!-- Carousel scripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>

  </body>

</html>
